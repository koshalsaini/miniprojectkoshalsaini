//
//  ViewController.m
//  foodmenu
//
//  Created by Click Labs 105 on 10/20/15.
//  Copyright (c) 2015 rohit. All rights reserved.
//

#import "ViewController.h"


@interface ViewController (){
    NSMutableArray *tableData;
    NSArray *recipes;

}
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UITableView *currentTableView;
@end


@implementation ViewController

@synthesize currentTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    tableData = [NSMutableArray new];
    tableData = [NSMutableArray arrayWithObjects:@"Valentines-Day-Raspberry-Napoleon-by.jpg",@"Moroccan-Stew-with-Chickpea-and-Butternut-Squash-.jpg",@"Lemon-Herb-Couscous-with-Almonds-by.jpg",@"Broiled-Grapefruit-w-GF-Streusel-Topping-by.jpg",nil];
   }
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    return 1;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:tableData [indexPath.row]];
    return cell;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showRecipeDetail"]) {
        NSIndexPath *indexPath = [self.tableViewindexPathForSelectedRow];
        secondViewController *destViewController = segue.destinationViewController;
        destViewController.uiimageview = [recipes objectAtIndex:indexPath.row];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
